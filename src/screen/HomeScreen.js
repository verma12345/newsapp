import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  FlatList,
} from 'react-native';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import News_Row from '../row_component/News_Row';

class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataSourse: [],
    }
  }

  componentDidMount() {
    fetch('http://newsapi.org/v2/everything?q=bitcoin&from=2020-12-21&sortBy=publishedAt&apiKey=23f4e6f598d1410ba9aa28398ac1469c')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ dataSourse: responseJson.articles })
      })
  }

  _renderItem = (dataItem) => {
    return (
      <News_Row
        data_item={dataItem.item}
      />
    )
  }
  
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.mainViewStyle}>

          <FlatList
            data={this.state.dataSourse}
            renderItem={this._renderItem}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },

  textStyle: {
    fontSize: 14,
    fontWeight: 'normal',
    marginVertical: 20,
    fontFamily: Font.SourceSansPro,
    color: '#1050E6',
  },
  mainViewStyle: {
    marginHorizontal: 80,
    alignItems: 'center',
  },
});
const mapStateToProps = (state) => {
  const ss = state.indexReducer;

  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {

    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
