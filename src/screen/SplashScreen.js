import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { StackActions } from '@react-navigation/native';

class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.navigation.dispatch(
      StackActions.replace('HomeScreen'),
    )
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.mainViewStyle}>

          <Text style={styles.textStyle}>{' LOADING...'}</Text>

        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },

  textStyle: {
    fontSize: 24,
    fontWeight: 'normal',
    marginVertical: 20,
    fontFamily: Font.SourceSansPro,
    color: '#1050E6',
  },
  mainViewStyle: {
    marginHorizontal: 80,
    alignItems: 'center',
  },
});
const mapStateToProps = (state) => {
  const ss = state.indexReducer;

  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {

    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
