import React from 'react'
import { Image, Text, View } from 'react-native'

const News_Row = (props) => {
    return (
        <View>
            <Text>
                {props.data_item.author}
            </Text>
            <Image
                source={{ uri: props.data_item.urlToImage }}
                style={{ height: 200, width: "100%" }}
            />
        </View>
    )
}

export default News_Row;